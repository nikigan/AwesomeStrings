﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace StringsItE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string phrase = textBox1.Text.ToLower().Replace(" ", String.Empty);
            if (Palindrom(phrase))
            {
                MessageBox.Show("Это палиндром!");
            }
            else
            {
                MessageBox.Show("Это не палиндром!");
            }
        }

        public bool Palindrom(string phrase)
        {
            
            string new_phrase = phrase.ToString().ToLower().Replace(" ", String.Empty);
            char[] reversed = new_phrase.ToCharArray();
            Array.Reverse(reversed);
            new_phrase = new string(reversed);
            return (phrase == new_phrase);
            
        }

        public int Count(string s)
        {
            
            return Regex.Matches(s, @"\w", RegexOptions.IgnoreCase).Count;
        }

        private void CountButton_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            MessageBox.Show(Convert.ToString(Count(s))+" букв в строке!");
        }

        public string NGlas(string str)
        {
            

            int countGlasnRu = Regex.Matches(str, @"[ауоыиэяюёе]", RegexOptions.IgnoreCase).Count;
            int countGlasnEn = Regex.Matches(str, @"[eyuioa]", RegexOptions.IgnoreCase).Count;
            int countGlasn = countGlasnEn + countGlasnRu;
            str = countGlasn.ToString();
            return str;

        }

        public string NSog(string str)
        {
           

            int countSogEn = Regex.Matches(str, @"[qwrtpsdfghjklzxvcbnm]", RegexOptions.IgnoreCase).Count;
            int countSogRu = Regex.Matches(str, @"[йфцчвскмпнртгьшлбщдзжхъ]", RegexOptions.IgnoreCase).Count;
            int countSogl = countSogRu + countSogEn;
            str = Convert.ToString(countSogl);
            return str;
            

        }

        private void toK_button_Click(object sender, EventArgs e)
        {
            string number = textBox1.Text;
            decimal n = numericUpDown_K.Value;
            double d = 0;
            if (Double.TryParse(number, out d))
            textBox1.Text = ToK(d, n);
            
        }

        public string ToK(double t, decimal n)
        {
            try
            {
                
                int k = (int)n;
                double result = Math.Round(t, k, MidpointRounding.AwayFromZero);
                return Convert.ToString(result);
            }
            catch
            {
                return "";
            }
           
        }

        private void button_Csezar_Click(object sender, EventArgs e)
        {
            int k = (int)numericUpDown_Csezar.Value; 
            textBox1.Text = csezarEncrypt(textBox1.Text, k); 
        }

        public string csezarEncrypt(string text, int k)
        {
            char[] oldString = text.ToCharArray();
            int p = k;
            string newString = "";
            char[] letters = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'к', 'л',
                'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ',
                'ы', 'ь', 'э', 'ю', 'я'};
            char[] eng_letters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
            for (int j = 0; j < oldString.Length; j++)
            {
                bool aLetter = false;
                bool isUpper = Char.IsUpper(oldString[j]);
                oldString[j] = Char.ToLower(oldString[j]);
                for (int i = 0; i < 32; i++)
                {
                    if (oldString[j] == letters[i])
                    {
                        aLetter = true;
                        while ((i + k) < 0)
                        {
                            k += 32;
                        }
                        newString += isUpper ? Char.ToUpper(letters[(i + k) % 32]) : letters[(i + k) % 32];
                    }
                }
                for (int i = 0; i < 26; i++)
                {
                    if (oldString[j] == eng_letters[i])
                    {
                        aLetter = true;
                        while ((i + p) < 0)
                        {
                            p += 26;
                        }
                        newString += isUpper ? Char.ToUpper(eng_letters[(i + p) % 26]) : eng_letters[(i + p) % 26];
                    }
                }
                if (!aLetter)
                    newString += oldString[j];
            }
            return newString;
        }

        private void butGlas_Click_1(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            MessageBox.Show("В сообщении " + NGlas(str) + " гласных!");

        }

        private void butSog_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            MessageBox.Show("В сообщении " + NSog(str) + " согласных!");
        }
    }
}
