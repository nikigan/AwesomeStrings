﻿namespace StringsItE
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numericUpDown_Csezar = new System.Windows.Forms.NumericUpDown();
            this.button_Csezar = new System.Windows.Forms.Button();
            this.numericUpDown_K = new System.Windows.Forms.NumericUpDown();
            this.toK_button = new System.Windows.Forms.Button();
            this.CountButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.butGlas = new System.Windows.Forms.Button();
            this.butSog = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Csezar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_K)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(64, 43);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(594, 77);
            this.textBox1.TabIndex = 0;
            // 
            // numericUpDown_Csezar
            // 
            this.numericUpDown_Csezar.Location = new System.Drawing.Point(546, 295);
            this.numericUpDown_Csezar.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDown_Csezar.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            -2147483648});
            this.numericUpDown_Csezar.Name = "numericUpDown_Csezar";
            this.numericUpDown_Csezar.Size = new System.Drawing.Size(46, 26);
            this.numericUpDown_Csezar.TabIndex = 7;
            // 
            // button_Csezar
            // 
            this.button_Csezar.Location = new System.Drawing.Point(394, 276);
            this.button_Csezar.Name = "button_Csezar";
            this.button_Csezar.Size = new System.Drawing.Size(145, 57);
            this.button_Csezar.TabIndex = 6;
            this.button_Csezar.Text = "Шифр Цезаря";
            this.button_Csezar.UseVisualStyleBackColor = true;
            this.button_Csezar.Click += new System.EventHandler(this.button_Csezar_Click);
            // 
            // numericUpDown_K
            // 
            this.numericUpDown_K.Location = new System.Drawing.Point(332, 295);
            this.numericUpDown_K.Name = "numericUpDown_K";
            this.numericUpDown_K.Size = new System.Drawing.Size(46, 26);
            this.numericUpDown_K.TabIndex = 5;
            // 
            // toK_button
            // 
            this.toK_button.Location = new System.Drawing.Point(180, 276);
            this.toK_button.Name = "toK_button";
            this.toK_button.Size = new System.Drawing.Size(145, 57);
            this.toK_button.TabIndex = 4;
            this.toK_button.Text = "Округление до k знаков";
            this.toK_button.UseVisualStyleBackColor = true;
            this.toK_button.Click += new System.EventHandler(this.toK_button_Click);
            // 
            // CountButton
            // 
            this.CountButton.Location = new System.Drawing.Point(180, 213);
            this.CountButton.Name = "CountButton";
            this.CountButton.Size = new System.Drawing.Size(145, 57);
            this.CountButton.TabIndex = 3;
            this.CountButton.Text = "Кол-во букв";
            this.CountButton.UseVisualStyleBackColor = true;
            this.CountButton.Click += new System.EventHandler(this.CountButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(180, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 57);
            this.button1.TabIndex = 1;
            this.button1.Text = "Палиндром";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(166, 164);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(63, 26);
            this.numericUpDown1.TabIndex = 5;
            // 
            // butGlas
            // 
            this.butGlas.Location = new System.Drawing.Point(394, 213);
            this.butGlas.Name = "butGlas";
            this.butGlas.Size = new System.Drawing.Size(145, 57);
            this.butGlas.TabIndex = 6;
            this.butGlas.Text = "N гласных";
            this.butGlas.UseVisualStyleBackColor = true;
            this.butGlas.Click += new System.EventHandler(this.butGlas_Click_1);
            // 
            // butSog
            // 
            this.butSog.Location = new System.Drawing.Point(394, 150);
            this.butSog.Name = "butSog";
            this.butSog.Size = new System.Drawing.Size(145, 57);
            this.butSog.TabIndex = 7;
            this.butSog.Text = "N согласных";
            this.butSog.UseVisualStyleBackColor = true;
            this.butSog.Click += new System.EventHandler(this.butSog_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 378);
            this.Controls.Add(this.butSog);
            this.Controls.Add(this.butGlas);
            this.Controls.Add(this.numericUpDown_Csezar);
            this.Controls.Add(this.button_Csezar);
            this.Controls.Add(this.numericUpDown_K);
            this.Controls.Add(this.toK_button);
            this.Controls.Add(this.CountButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "AwesomeStrings";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Csezar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_K)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown_Csezar;
        private System.Windows.Forms.Button button_Csezar;
        private System.Windows.Forms.NumericUpDown numericUpDown_K;
        private System.Windows.Forms.Button toK_button;
        private System.Windows.Forms.Button CountButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button butGlas;
        private System.Windows.Forms.Button butSog;
    }
}

