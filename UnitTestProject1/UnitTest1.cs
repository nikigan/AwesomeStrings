﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringsItE;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        Form1 f = new Form1();

        [TestMethod]
        public void PalindromTestTrue()
        {
            string test = "testtset";
            Assert.IsTrue(f.Palindrom(test));
        }

        [TestMethod]
        public void PalindromTestFalse()
        {
            string test = "aqwsedrftgyhuj";
            Assert.IsFalse(f.Palindrom(test));
        }

        [TestMethod]
        public void Count10()
        {
            string t = "a::--aa,aaa,aa..aa";
            Assert.AreEqual(f.Count(t), 10);
        }

        [TestMethod]
        public void Count0()
        {
            string t = "";
            Assert.AreEqual(f.Count(t), 0);
        }

        [TestMethod]
        public void NGlas0()
        {
            string t = "";
            Assert.AreEqual(f.NGlas(t), "0");
        }

        [TestMethod]
        public void NGlas5()
        {
            string t = "AsddssdaOoEkklkl";
            Assert.AreEqual(f.NGlas(t), "5");
        }

        [TestMethod]
        public void NSog0()
        {
            string t = "";
            Assert.AreEqual(f.NSog(t), "0");
        }

        [TestMethod]
        public void NSog7()
        {
            string t = "aaaaAaaasssDFeeeRK";
            Assert.AreEqual(f.NSog(t), "7");
        }
    }
}
